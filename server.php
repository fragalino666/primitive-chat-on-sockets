<?php
define('PORT', '8090');

require_once ("classes/Chat.php");
$empty_array = [];

$chat = new Chat();

$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
socket_set_option($socket, SOL_SOCKET, SO_REUSEADDR, 1);
socket_bind($socket, 0, PORT);
socket_listen($socket);

$clientSocketArray = [$socket];

while(true) {

    $newSocketArray = $clientSocketArray;
    socket_select($newSocketArray, $empty_array, $empty_array,0, 10);

    if(in_array($socket, $newSocketArray)){
        $new_socket = socket_accept($socket);
        $clientSocketArray[] = $new_socket;

        $header = socket_read($new_socket, 1024);
        $chat->sendHeaders($header, $new_socket, 'localhost/chat-test', PORT);

        socket_getpeername($new_socket, $client_ip_address);
        echo $client_ip_address;

        $connectionACK = $chat->newConnectionACK($client_ip_address);
        $chat->send($connectionACK, $clientSocketArray);

        $newSocketArrayIndex = array_search($socket, $newSocketArray);
        unset($newSocketArray[$newSocketArrayIndex]);

    }

    foreach($newSocketArray as $resourse) {

        while(socket_recv($resourse, $data, 1024, 0) >= 1){
            $socketMessage = $chat->unseal($data);
            $messageObj = json_decode($socketMessage);

            // var_dump($messageObj);
            if(isset($messageObj->chat_user)) {
                $chatMessage = $chat->create_chat_message($messageObj->chat_user, $messageObj->chat_message);
                $chat->send($chatMessage, $clientSocketArray);
            }

            break 2;
        }

        $socketData = @socket_read($resourse, 1024, PHP_NORMAL_READ);
        if($socketData === false) {
            socket_getpeername($resourse, $client_ip_address);
            $connectionACK = $chat->newDisconnectedACK($client_ip_address);
            $chat->send($connectionACK, $clientSocketArray);

            $newSocketArrayIndex = array_search($resourse, $clientSocketArray);
            unset($clientSocketArray[$newSocketArrayIndex]);
        }

    }

}

socket_close($socket);