<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Chat test</title>
    
    <link rel="stylesheet" href="style/reset.css" />
    <link rel="stylesheet" href="style/style.css" />
</head>
<body>
    
    <section class="chat_block">
        <div class="container">
            <form id="chat_form" action="#">
                <input placeholder="your name" id="username" type="text">
                <div class="chat-result" id="chat-result">

                </div>
                <div class="chat_fields flex-start-between">
                    <textarea placeholder="Message" id="message"></textarea>
                    <input type="submit" value="Send">
                </div>
            </form>
        </div>
    </section>

    <script src="script/functions.js"></script>
    <script src="script/script.js"></script>
</body>
</html>