
// document.onload = function(){
    // }
    
document.addEventListener('DOMContentLoaded', function(){
    
    var socket = new WebSocket('ws://localhost:8090/chat-test/server.php');

    

    socket.onopen = function(){
        sys_message('Connected to chat');
    }

    socket.onerror = function(error){
        sys_message('Fatal: ' + error.message);
    }

    socket.onclose = function(){
        sys_message('Connection closed');
    }

    socket.onmessage = function(event){
        var data = JSON.parse(event.data);
        console.log('=========================================');
        console.log(event);
        console.log(data);
        console.log('=========================================');

        switch (data.type) {
            case 'newConnectionACK':
                // sys_message(data.message);
                break;

            case 'user_message':
                user_message(data.message);
                break;
        
            default:
                message(data.message);
        }

    }

    document.getElementById('chat_form').addEventListener('submit', function(e){
        e.preventDefault();
        if(document.getElementById('message').value.replace(/^\s+|\s+$/g, '') != '') {
            var message = {
                chat_message: document.getElementById('message').value,
                chat_user: document.getElementById('username').value
            }
    
            socket.send(JSON.stringify(message));
            document.getElementById('message').value = '';
        }
    });

    document.getElementById('message').addEventListener('keypress', function(e){
        if (e.key === "Enter") {
            document.getElementById('chat_form').dispatchEvent(new Event('submit'));
            setTimeout(function(){
                document.getElementById('message').value = '';
            },0);
        }
    });

})