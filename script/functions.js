function sys_message(text){
    var elem = document.createElement('div');
    elem.innerHTML = text;
    elem.classList='chat_item message';
    document.getElementById('chat-result').append(elem);
}

function message(text){
    var elem = document.createElement('div');
    elem.innerHTML = text;
    elem.classList='chat_item';
    document.getElementById('chat-result').append(elem);
}

function user_message(text){
    var elem = document.createElement('div');
    var message = JSON.parse(text);
    elem.innerHTML = `<div class="flex-center-between">
        <div class="chat_username">${message.user}</div>
        <div class="chat_message">${message.message}</div>
    </div>`;
    elem.classList='chat_item';
    document.getElementById('chat-result').append(elem);
}