<?php

class Chat {

    public function sendHeaders($input, $new_socket, $host, $port){
        $tmpLine = preg_split("/\r\n/", $input);
        $key = '';


        foreach ($tmpLine as $item) {
            if(strpos($item, 'Sec-WebSocket-Key') !== false) {
                $key = explode(': ',trim($item))[1];
            }
        }

        $result_key = base64_encode(
            pack('H*', sha1($key.'258EAFA5-E914-47DA-95CA-C5AB0DC85B11'))
        );

        $headers = "HTTP/1.1 101 Switching Protocols \r\n".
            "Upgrade: websocket\r\n".
            "Connection: Upgrade\r\n".
            "WebSocket-Origin: $host\r\n".
            "WebSocket-Location: ws://$host:$port/chat-test/server.php\r\n".
            "Sec-WebSocket-Accept:$result_key\r\n\r\n"
        ;

        // echo $headers;

        socket_write($new_socket, $headers, strlen($headers));
    }

    public function newConnectionACK($ip){
        $message_array = [
            'message' => 'new client ' . $ip . ' connected',
            'type' => 'newConnectionACK'
        ];

        return $this->seal(json_encode($message_array));
    }

    public function newDisconnectedACK($ip){
        $message_array = [
            'message' => 'client ' . $ip . ' disconnected',
            'type' => 'newConnectionACK'
        ];

        return $this->seal(json_encode($message_array));
    }

    public function send($message, $clientSocketArray){
        $message_length = strlen($message);

        foreach($clientSocketArray as $clientSocket) {
            @socket_write($clientSocket, $message, $message_length);
        }

        return true;
    }

    public function seal($json){
        $b1 = 0x81;
        $length = strlen($json);
        $header = '';

        if($length<=125){
            $header = pack('CC', $b1, $length);
        } else if ($length > 125 && $length < 65536) {
            $header = pack('CCn', $b1, 126, $length);
        } else if($length >= 65536 ) {
            $header = pack('CCNN', $b1, 127, $length);
        }

        return $header.$json;
    }

    public function unseal($str){
        $length = ord($str[1]) & 127;

        if($length == 126) {
            $mask = substr($str, 4, 4);
            $data = substr($str, 8);
        } else if ($length == 127) {
            $mask = substr($str, 10, 4);
            $data = substr($str, 14);
        } else {
            $mask = substr($str, 2, 4);
            $data = substr($str, 6);
        }

        $result = '';

        for($i = 0; $i< strlen($data); $i++) {
            $result.= $data[$i] ^ $mask[$i%4];
        }

        return $result;
    }

    public function create_chat_message($username, $message_str){
        $message = json_encode(['user'=> $username, 'message'=> $message_str]);
        $message_array = [
            'type'=> 'user_message',
            'message' => $message
        ];

        return $this->seal(json_encode($message_array));
    }

}